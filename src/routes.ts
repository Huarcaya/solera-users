import express from "express";

import authRouters from "./auth/auth.routes";
import usersRouters from "./users/users.routes";

const routesApi = (app: any) => {
  const router = express.Router();
  app.use("/api/v1", router);

  router.use("/auth", authRouters);
  router.use("/users", usersRouters);
};

export default routesApi;
