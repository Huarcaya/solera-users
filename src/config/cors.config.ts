import { CorsOptions } from "cors";

const allowedOrigins = ["http://localhost:4200"];
export const corsOptions: CorsOptions = {
  origin: allowedOrigins,
};
