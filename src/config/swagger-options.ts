import path from "path";

import { TheApp } from ".";

export const swaggerOptions = {
  definition: {
    openapi: "3.0.3",
    info: {
      title: `${TheApp.name}`,
      version: `${TheApp.version}`,
      description: `The ${TheApp.name}' API.`,
    },
  },
  apis: [
    `${path.dirname(path.dirname(__filename))}/**/*.routes.{js,ts}`,
    `${path.dirname(path.dirname(__filename))}/**/routes/*.routes.{js,ts}`,
  ],
};
