import Joi from "joi";

import { TheApp } from ".";

export const validationAppSchema = () => {
  const envSchema = Joi.object({
    APP_NAME: Joi.string().required(),
    NODE_ENV: Joi.string()
      .valid(TheApp.env.dev, TheApp.env.test, TheApp.env.stag, TheApp.env.prod)
      .default(TheApp.env.dev),
    PORT: Joi.number().required(),
    MYSQL_DB_HOST: Joi.string().required(),
    MYSQL_DB_PORT: Joi.string().required(),
    MYSQL_DB_NAME: Joi.string().required(),
    MYSQL_DB_USER: Joi.string().required(),
    MYSQL_DB_PASSWORD: Joi.string().required(),
  }).unknown();

  const { error } = envSchema.validate(process.env, { abortEarly: false });

  if (error) {
    throw new Error(`Config validation error: ${error.message}`);
  }
};
