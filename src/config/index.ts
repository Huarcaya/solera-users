import "dotenv/config";

export const TheApp = {
  name: process.env.APP_NAME,
  port: process.env.PORT,
  version: "0.1.0",
  api: {
    v1: "v1",
    v2: "v2",
  },
  env: {
    dev: "development",
    test: "testing",
    stag: "staging",
    prod: "production",
  },
};

const config = {
  interviewDb: {
    dbHost: process.env.MYSQL_DB_HOST,
    dbPort: process.env.MYSQL_DB_PORT || 3306,
    dbName: process.env.MYSQL_DB_NAME,
    dbUser: process.env.MYSQL_DB_USER,
    dbPassword: process.env.MYSQL_DB_PASSWORD,
  },
};

export { validationAppSchema } from "./validation.schema";

export default config;
