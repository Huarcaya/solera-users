import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUI from 'swagger-ui-express';

import { swaggerOptions } from '../../config/swagger-options';
import { TheApp } from '../../config';

const specs = swaggerJSDoc(swaggerOptions);

export const swaggerRoutes = (app: any) => {
  if (process.env.NODE_ENV != TheApp.env.prod) {
    app.use('/docs', swaggerUI.serve, swaggerUI.setup(specs));
  }
};
