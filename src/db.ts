import { DataSource } from "typeorm";

import config from "./config";

export const AppDataSource = new DataSource({
  type: "mysql",
  host: config.interviewDb.dbHost,
  username: config.interviewDb.dbUser,
  password: config.interviewDb.dbPassword,
  database: config.interviewDb.dbName,
  entities: [
    __dirname + "/entities/mysql/*.entity.{js,ts}",
    __dirname + "/**/entities/mysql/*.entity.{js,ts}",
  ],
});
