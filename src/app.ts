import cors from "cors";
import express from "express";

import {
  boomErrorHandler,
  errorHandler,
} from "./core/middlewares/error.handler";
import { swaggerRoutes } from "./core/routes/swagger.routes";
import routesApi from "./routes";

const app = express();

app.use(cors());
app.use(express.json({ limit: "4MB" }));
app.use(express.urlencoded({ extended: true }));

app.get("/", (req: any, res: any) => {
  res.json({ message: "OK" });
});

routesApi(app);
swaggerRoutes(app);

// Error Middlewares
app.use(boomErrorHandler);
app.use(errorHandler);

export default app;
