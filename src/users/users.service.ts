import bcrypt from "bcrypt";
import boom from "@hapi/boom";

import { AppDataSource } from "../db";
import { CreateNewUserDto, UserCreatedDto } from "./user.dto";
import { UserEntity } from "./entities/mysql/user.entity";

export class UsersService {
  constructor(
    private _userRepository = AppDataSource.getRepository(UserEntity)
  ) {}

  async findAllUsers(): Promise<UserEntity[]> {
    const users = await this._userRepository.find();

    users.forEach((i) => {
      delete i.password;
      delete i.createdAt;
      delete i.updatedAt;
    });

    return users;
  }

  async findOneByUsername(username: string): Promise<UserEntity> {
    const user = await this._userRepository.findOneBy({ username });

    if (!user) {
      throw boom.notFound("User not found");
    }

    return user;
  }

  async saveNewUser(bodyPayload: CreateNewUserDto): Promise<UserCreatedDto> {
    const hashedPassword = await bcrypt.hash(bodyPayload.password, 12);

    const newUser = new UserEntity();
    newUser.username = bodyPayload.username;
    newUser.password = hashedPassword;
    newUser.fullName = bodyPayload.fullName;

    const { username } = await this._userRepository.save(newUser);

    return { username };
  }
}
