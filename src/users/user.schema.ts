import Joi from "joi";

const username = Joi.string().required();
const password = Joi.string().required();
const fullName = Joi.string();

export const createNewUserSchema = Joi.object({
  username,
  password,
  fullName,
});
