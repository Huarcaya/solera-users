import { NextFunction, Request, Response } from "express";

import { UsersService } from "./users.service";

const usersService = new UsersService();

export const getUsers = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const users = await usersService.findAllUsers();

    res.json({ data: users });
  } catch (error) {
    next(error);
  }
};

export const createNewUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const data = await usersService.saveNewUser(req.body);

    if (data?.username) {
      res.status(201);
    }

    res.json({ data });
  } catch (error) {
    next(error);
  }
};
