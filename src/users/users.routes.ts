import { Router } from "express";

import { createNewUser, getUsers } from "./users.controller";
import { validatorHandler } from "../core/middlewares/validator.handler";
import { createNewUserSchema } from "./user.schema";

const router = Router();

/**
 * @openapi
 * /api/v1/users:
 *   get:
 *     summary: Get a list of users
 *     description: Get a list of users
 *     tags: [users]
 *     responses:
 *       200:
 *         description: OK
 *       404:
 *         description: Not Found
 */
router.get("/", getUsers);

/**
 * @openapi
 * /api/v1/users:
 *   post:
 *     summary: Register a new user
 *     description: Register a new user
 *     tags: [users]
 *     requestBody:
 *       content:
 *         application/x-www-form-urlencoded:
 *           schema:
 *             type: object
 *             properties:
 *               username:
 *                 type: string
 *               password:
 *                 type: string
 *                 format: password
 *               fullName:
 *                 type: string
 *             required:
 *               - username
 *               - password
 *     responses:
 *       201:
 *         description: Created
 */
router.post("/", validatorHandler(createNewUserSchema, "body"), createNewUser);

export default router;
