export type CreateNewUserDto = {
  username: string;
  password: string;
  fullName: string;
};

export type UserCreatedDto = {
  username: string;
};

export type UserLoggedDto = {
  fullName: string;
};
