import "reflect-metadata";

import { AppDataSource } from "./db";
import { TheApp, validationAppSchema } from "./config";
import app from "./app";

async function main() {
  try {
    validationAppSchema();
    const { name: appName, port } = TheApp;
    await AppDataSource.initialize();
    app.listen(port, () => {
      console.log(`${appName} is listening on port ${port}`);
    });
  } catch (error) {
    console.log(error);
  }
}

main();
