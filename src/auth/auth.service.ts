import bcrypt from "bcrypt";
import boom from "@hapi/boom";

import { UsersService } from "../users/users.service";

export class AuthService {
  constructor(private _usersService = new UsersService()) {}

  async login(username: string, password: string): Promise<string> {
    const user = await this._usersService.findOneByUsername(username);

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
      throw boom.unauthorized("Bad credentials.");
    }

    const { fullName } = user;

    return fullName;
  }
}
