import { Router } from "express";

import { login } from "./auth.controller";
import { loginSchema } from "./auth.schema";
import { validatorHandler } from "../core/middlewares/validator.handler";

const router = Router();

/**
 * @openapi
 * /api/v1/auth:
 *   post:
 *     summary: User Login
 *     description: User authentication
 *     tags: [auth]
 *     requestBody:
 *       content:
 *         application/x-www-form-urlencoded:
 *           schema:
 *             type: object
 *             properties:
 *               username:
 *                 type: string
 *               password:
 *                 type: string
 *                 format: password
 *             required:
 *               - username
 *               - password
 *     responses:
 *       200:
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 data:
 *                   type: object
 *                   properties:
 *                     fullName:
 *                       type: string
 *       401:
 *         description: Unauthorized
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 statusCode:
 *                   type: integer
 *                   example: 401
 *                 error:
 *                   type: string
 *                   example: Unauthorized
 *                 message:
 *                   type: string
 *                   description: Unauthorized
 */
router.post("/", validatorHandler(loginSchema, "body"), login);

export default router;
