import { NextFunction, Request, Response } from "express";

import { AuthService } from "./auth.service";

const authService = new AuthService();

export const login = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { username, password } = req.body;

    const fullName = await authService.login(username, password);

    res.json({ data: { fullName } });
  } catch (error) {
    next(error);
  }
};
