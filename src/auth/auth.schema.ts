import Joi from "joi";

const username = Joi.string().required();
const password = Joi.string().required();

export const loginSchema = Joi.object({
  username,
  password,
});
