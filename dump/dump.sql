create table users(
    id int primary key auto_increment,
    user varchar(255) not null,
    password varchar(255) not null,
    full_name varchar(255),
    created_at timestamp not null default current_timestamp(),
    updated_at timestamp
);