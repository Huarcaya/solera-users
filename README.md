# solera-users

This is a demo of a REST web service using Node.js with TypeScript.

Some additional libraries were used, added in the `package.json` file, to:

- Validate required secrets in `.env` file.
- Validate required input fields in the APIs.
- Show appropriate HTTP Status Code and manage error handling.
- Use OpenAPI specification.
- Use a TypeScript ORM.
- Encrypt user passwords.

# endpoints

The project runs locally at: `localhost:3000`.

The following routes are available:

- [GET] `/docs`: Show all available API routes using OpenAPI/Swagger.
- [POST] `/api/v1/auth`: Endpoint to login to the service.
    - It's necessary to send a json with `username` and `password` properties.
- [GET] `/api/v1/users`: Shows a list of registered users.
- [POST] `/api/v1/users`: Endpoint to register a new user.
    - It's necessary to send a json with `username` and `password` properties, and if you want to register the user's name, there is a third optional property called `fullName`.

> Note: There is information about the required fields and also a way to test these APIs in `/docs`.

# settings

## .env file

The project uses an env file to get the secrets needed for the application.

Rename `.env.sample` to `.env` and change the values according your env settings.

```sh
cp .env.sample .env
```

## database

The project was developed using MariaDB as RDBMS inside a Docker container.

The database service uses the values of our `.env` file and also creates the users table from the script located in the `dump` dir.

*Before to execute the following command, make sure you are not using port 3306:*

```sh
sudo docker-compose --env-file .env up -d
```

> Note: If you don't want to use Docker because you already have a MySQL/MariaDB installation, you can run the `./dump/dump.sql` script directly in your database terminal to create the tables required by the project.

## install project dependencies

The project uses `npm` as Node.js package manager, so to install the project dependencies, execute:

```sh
npm install
```

## run the project

### dev mode

In dev mode, there is a watcher which is listening the changes in TypeScript code to serve these changes in the local server.

```sh
npm run start:dev
```

### prod mode

```sh
npm run build
npm run start:prod
```

# extra tools

## adminer

There is an instance of `adminer` that can be used as a client to access the database. This instance of `adminer` is also running with Docker.

Open your browser, paste the next URL `localhost:8080`, and, if you didn't change anything in the `.env` file, put the following info:

```txt
System: MySQL
Server: mariadb
Username: interview
Password: interview123
Database: interview_db
```